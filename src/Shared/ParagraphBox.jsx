import React from 'react';
import PropTypes from 'prop-types';

const ParagraphBox = ({ onChange, value, name }) => {
  return <textarea value={value} onChange={onChange} name={name} />;
};

ParagraphBox.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  name: PropTypes.string
};

ParagraphBox.defaultProps = {
  value: '',
  name: ''
};

export default ParagraphBox;
