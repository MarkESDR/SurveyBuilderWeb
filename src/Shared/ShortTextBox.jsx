import React from 'react';
import PropTypes from 'prop-types';

const ShortTextBox = ({ onChange, value, name }) => {
  return <input name={name} type="text" value={value} onChange={onChange} />;
};

ShortTextBox.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  name: PropTypes.string
};

ShortTextBox.defaultProps = {
  value: '',
  name: ''
};

export default ShortTextBox;
