import React from 'react';
import PropTypes from 'prop-types';

const RadioOptions = ({
  onChange, value, options, name
}) => {
  return (
    <form>
      {options.map((option) => {
        const { value: optionValue, id: optionId } = option;
        return (
          <label htmlFor={optionId} key={optionId} style={{ display: 'block' }}>
            <input
              type="radio"
              id={optionId}
              value={optionValue}
              checked={optionValue === value}
              onClick={onChange}
              name={name}
            />
            {optionValue}
          </label>
        );
      })}
    </form>
  );
};
RadioOptions.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.node,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  })).isRequired,
  name: PropTypes.string
};
RadioOptions.defaultProps = {
  value: '',
  name: ''
};
export default RadioOptions;
