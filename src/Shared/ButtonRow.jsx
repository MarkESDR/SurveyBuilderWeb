import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

const ButtonRow = ({ buttons }) => (
  <div className="button-row">
    {buttons.map((buttonProps) => {
      const { id, text } = buttonProps;
      return <Button key={id || text} {...buttonProps} />;
    })}
  </div>
);

ButtonRow.propTypes = {
  buttons: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    text: PropTypes.string
  })).isRequired
};

export default ButtonRow;
