import React from 'react';
import PropTypes from 'prop-types';

const OrderedList = (props) => {
  const { elements } = props;
  return <ol>{elements.map(({ value, id }) => <li key={id}>{value}</li>)}</ol>;
};

OrderedList.propTypes = {
  elements: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.node,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  })).isRequired
};

export default OrderedList;
