import React from 'react';
import PropTypes from 'prop-types';

const DropdownOptions = ({
  onChange, options, value, name
}) => {
  return (
    <select value={value} onChange={onChange} name={name}>
      {options.map(({ value: optionValue, id: optionId }) => {
        return (
          <option value={optionValue} key={optionId}>
            {optionValue}
          </option>
        );
      })}
    </select>
  );
};
DropdownOptions.propTypes = {
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.node,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  })).isRequired,
  value: PropTypes.string,
  name: PropTypes.string
};
DropdownOptions.defaultProps = {
  value: '',
  name: ''
};
export default DropdownOptions;
