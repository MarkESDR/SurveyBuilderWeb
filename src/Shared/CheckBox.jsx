import React from 'react';
import PropTypes from 'prop-types';

const CheckBox = ({
  onChange, label, checked, name
}) => (
  <label htmlFor={name}>
    <input name={name} type="checkbox" id={name} checked={checked} onChange={onChange} />
    {label}
  </label>
);

CheckBox.propTypes = {
  onChange: PropTypes.func.isRequired,
  label: PropTypes.node,
  checked: PropTypes.bool,
  name: PropTypes.string
};
CheckBox.defaultProps = {
  label: 'This needs a label',
  checked: false,
  name: ''
};
export default CheckBox;
