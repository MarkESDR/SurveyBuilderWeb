import React, { Component } from 'react';

import SurveyElementSelector from '../EditSurvey/SurveyElementSelector';
import SurveyPresenter from './SurveyPresenter';
import { ELEMENT_COMPONENT_IDS_BY_NAME, ELEMENT_COMPONENT_IDS } from '../Constants/Element';
import { createSurveyElement } from '../Helper';

export default class SurveyContainer extends Component {
  state = {
    elementList: []
  };

  onChange = (e) => {
    const { elementList } = this.state;
    const updatedElementList = elementList.map((element) => {
      const { elementId, elementComponentId } = element;
      let newValue;
      if (elementComponentId === ELEMENT_COMPONENT_IDS.CHECK_BOX) {
        newValue = e.target.checked ? 'true' : '';
      } else {
        newValue = e.target.value;
      }
      if (elementId === e.target.name) {
        return {
          ...element,
          value: newValue
        };
      }
      return element;
    });
    this.setState({
      ...this.state,
      elementList: updatedElementList
    });
  };

  onAdd = (elementName) => {
    const { elementList: updatedElementList } = this.state;
    updatedElementList.push(createSurveyElement(ELEMENT_COMPONENT_IDS_BY_NAME[elementName]));
    this.setState({
      ...this.state,
      elementList: updatedElementList
    });
  };

  render = () => {
    const { elementList } = this.state;
    return (
      <div>
        <SurveyElementSelector onSubmit={this.onAdd} />
        <SurveyPresenter elements={elementList} onChange={this.onChange} />
      </div>
    );
  };
}
