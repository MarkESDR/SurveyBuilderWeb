import React from 'react';

import {
  ELEMENT_DEFAULT_PROP_TYPES,
  BLANK_PROP_VALUES,
  ELEMENT_COMPONENTS_BY_ID
} from '../Constants/Element';

const SurveyElement = (props) => {
  const { elementTitle, elementComponentId, elementId } = props;
  const ElementComponent = ELEMENT_COMPONENTS_BY_ID[elementComponentId];
  const titleComponent = <p>{elementTitle}</p>;
  return (
    <div className="survey-wrapper" style={{ display: 'block' }}>
      {titleComponent}
      <ElementComponent {...props} name={elementId} />
    </div>
  );
};

SurveyElement.propTypes = ELEMENT_DEFAULT_PROP_TYPES;
SurveyElement.defaultProps = BLANK_PROP_VALUES;

export default SurveyElement;
