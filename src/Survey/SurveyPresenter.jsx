import React from 'react';
import PropTypes from 'prop-types';
import { partialRight } from 'lodash';

import { ELEMENT_DEFAULT_PROP_TYPES } from '../Constants/Element';
import SurveyElement from './SurveyElement';

const SurveyPresenter = ({ elements, onChange }) => (
  <div>
    {elements.map((props) => {
      const { elementId, value } = props;
      return (
        <SurveyElement
          {...props}
          key={elementId}
          onChange={partialRight(onChange, elementId)}
          name={elementId}
          checked={!!value}
        />
      );
    })}
  </div>
);

SurveyPresenter.propTypes = {
  elements: PropTypes.arrayOf(PropTypes.shape(ELEMENT_DEFAULT_PROP_TYPES)).isRequired,
  onChange: PropTypes.func.isRequired
};

export default SurveyPresenter;
