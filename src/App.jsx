import React, { Component } from 'react';
import { find, findIndex, set } from 'lodash';

import { PAGE_STATE } from './Constants/Page';
import EditSurveyPage from './EditSurvey/EditSurveyPage';
import SurveyMenuPage from './SurveyMenu/SurveyMenuPage';
import Survey from './Survey/SurveyContainer';
import { createSurvey } from './Helper';

export class App extends Component {
  state = {
    surveys: [],
    currentSurveyId: undefined,
    pageState: PAGE_STATE.SURVEY_MENU
  };

  onEditSurvey = surveyId => this.changeSurveyState(PAGE_STATE.EDIT_SURVEY, surveyId);

  onAnswerSurvey = surveyId => this.changeSurveyState(PAGE_STATE.ANSWER_SURVEY, surveyId);

  onGoToMenu = () => this.changeSurveyState(PAGE_STATE.SURVEY_MENU);

  onCreateSurvey = () => {
    const { surveys } = this.state;
    const newSurvey = createSurvey();
    surveys.splice(0, 0, newSurvey);
    this.setState({
      ...this.state,
      surveys
    });
  };

  changeSurveyState = (newPageState, surveyId) => {
    const { surveys } = this.state;
    switch (newPageState) {
      case PAGE_STATE.ANSWER_SURVEY:
      // falls through
      case PAGE_STATE.EDIT_SURVEY:
        if (surveys.map(survey => survey.surveyId).includes(surveyId)) {
          this.setState({
            ...this.state,
            currentSurveyId: surveyId,
            pageState: newPageState
          });
          break;
        }
      // falls through
      default:
        this.setState({
          ...this.state,
          currentSurveyId: undefined,
          pageState: PAGE_STATE.SURVEY_MENU
        });
    }
  };

  saveCurrentSurvey = (newElements, newName) => {
    const { surveys, currentSurveyId } = this.state;
    if (!currentSurveyId) {
      return;
    }

    const currentSurveyIndex = findIndex(surveys, { surveyId: currentSurveyId });
    set(surveys, `[${currentSurveyIndex}].elements`, newElements);
    set(surveys, `[${currentSurveyIndex}].name`, newName);

    this.setState({
      ...this.state,
      surveys
    });
  };

  render() {
    const { surveys, currentSurveyId, pageState } = this.state;
    if (surveys.length < 1 || !currentSurveyId || pageState === PAGE_STATE.ANSWER_SURVEY) {
      return (
        <SurveyMenuPage surveys={surveys} onCreateSurvey={this.onCreateSurvey} onEditSurvey={this.onEditSurvey} />
      );
    }

    const currentSurvey = find(surveys, { surveyId: currentSurveyId });
    if (pageState === PAGE_STATE.EDIT_SURVEY) {
      return (
        <EditSurveyPage
          {...currentSurvey}
          onGoToMenu={this.onGoToMenu}
          saveSurvey={this.saveCurrentSurvey}
        />
      );
    }
    if (pageState === PAGE_STATE.ANSWER_SURVEY) {
      return <Survey {...currentSurvey} />;
    }
  }
}
