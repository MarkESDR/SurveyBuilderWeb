import React, { Component } from 'react';
import PropTypes from 'prop-types';

import EditSurveyElementButtonRow from './EditSurveyElementButtonRow';
import { DropdownOptions, Button } from '../Shared';
import { ELEMENT_COMPONENT_NAMES_ARRAY } from '../Constants/Element';

export default class SurveyElementSelector extends Component {
  static propTypes = {
    submit: PropTypes.func.isRequired,
    moveElementUp: PropTypes.func,
    moveElementDown: PropTypes.func,
    index: PropTypes.number
  };

  static defaultProps = {
    index: 0,
    moveElementDown: undefined,
    moveElementUp: undefined
  };

  state = {
    value: ELEMENT_COMPONENT_NAMES_ARRAY[0]
  };

  onChange = (e) => {
    this.setState({
      ...this.state,
      value: e.target.value
    });
  };

  onClick = () => {
    this.props.submit(this.state.value);
  };

  render = () => {
    const options = ELEMENT_COMPONENT_NAMES_ARRAY.map((name, i) => {
      return {
        value: name,
        id: i
      };
    });
    return (
      <div className="element-selector">
        <DropdownOptions
          onChange={this.onChange}
          options={options}
          value={this.state.value}
        />
        <Button onClick={this.onClick} text="Select" />
        <EditSurveyElementButtonRow {...this.props} />
      </div>
    );
  };
}
