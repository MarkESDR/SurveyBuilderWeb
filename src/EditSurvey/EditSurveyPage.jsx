import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { generateId } from '../Helper';
import { ELEMENT_DEFAULT_PROP_TYPES } from '../Constants/Element';
import EditSurveyContainer from './EditSurveyContainer';
import { CheckBox, Button } from '../Shared';

class EditSurveyPage extends Component {
  static propTypes = {
    elements: PropTypes.arrayOf(PropTypes.shape(ELEMENT_DEFAULT_PROP_TYPES)),
    name: PropTypes.string,
    onGoToMenu: PropTypes.func,
    saveSurvey: PropTypes.func.isRequired
  };

  static defaultProps = {
    elements: [],
    name: '',
    onGoToMenu: undefined
  };

  constructor(props) {
    super(props);

    const { elements, name } = props;
    this.state = {
      checkBoxName: generateId(),
      tempElements: elements,
      tempName: name,
      editing: true
    };
  }

  onToggleEdit = () => {
    const { editing } = this.state;

    this.setState({
      ...this.state,
      editing: !editing
    });
  };

  onSave = () => {
    const { tempElements, tempName } = this.state;
    this.props.saveSurvey(tempElements, tempName);
  };

  updateElements = (elements) => {
    this.setState({
      ...this.state,
      tempElements: elements
    });
  };

  updateName = (name) => {
    this.setState({
      ...this.state,
      tempName: name
    });
  };

  render() {
    const { elements, onGoToMenu, name } = this.props;
    const { editing, checkBoxName } = this.state;
    return (
      <div>
        {onGoToMenu && <Button text="Go back to Survey Menu" onClick={onGoToMenu} />}
        <CheckBox
          onChange={this.onToggleEdit}
          label="Preview"
          checked={!editing}
          name={checkBoxName}
        />
        <Button text="Save" onClick={this.onSave} />
        <EditSurveyContainer
          elements={elements}
          editing={editing}
          name={name}
          updateElements={this.updateElements}
          updateName={this.updateName}
        />
      </div>
    );
  }
}

export default EditSurveyPage;
