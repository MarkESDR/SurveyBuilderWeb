import React, { Component } from 'react';
import { pick, noop, set, get } from 'lodash';
import PropTypes from 'prop-types';

import { ShortTextBox, OrderedList } from '../Shared';
import { generateId } from '../Helper';
import EditSurveyElementButtonRow from './EditSurveyElementButtonRow';
import {
  ELEMENT_DEFAULT_PROPS,
  ELEMENT_DEFAULT_PROP_TYPES,
  ELEMENT_COMPONENT_IDS
} from '../Constants/Element';
import SurveyElement from '../Survey/SurveyElement';

export default class EditSurveyElement extends Component {
  static propTypes = {
    ...ELEMENT_DEFAULT_PROP_TYPES,
    moveElementUp: PropTypes.func,
    moveElementDown: PropTypes.func,
    index: PropTypes.number.isRequired,
    changeElementData: PropTypes.func.isRequired,
    deleteElement: PropTypes.func,
    editing: PropTypes.bool
  };

  static defaultProps = {
    deleteElement: undefined,
    moveElementUp: undefined,
    moveElementDown: undefined,
    editing: true
  };

  state = {
    newOptionId: generateId()
  };

  onChangeElementData = (e) => {
    const { elementId } = this.props;
    const { newOptionId } = this.state;
    const elementData = pick(this.props, ELEMENT_DEFAULT_PROPS);
    set(elementData, e.target.name, e.target.value);

    // If changed data is a new option, refresh newOptionId
    elementData.options = elementData.options.map((option) => {
      if (get(option, 'id')) {
        return option;
      }
      this.setState({
        ...this.state,
        newOptionId: generateId()
      });
      return { ...option, id: newOptionId };
    });

    this.props.changeElementData(elementId, elementData);
  };

  getDataRenders = () => {
    const {
      elementComponentId, elementTitle, label, options
    } = this.props;

    const elementTitleBox = this.renderTextField(elementTitle, 'elementTitle');
    const labelBox = this.renderTextField(label, 'label');
    let optionsBoxes;

    switch (elementComponentId) {
      case ELEMENT_COMPONENT_IDS.RADIO_OPTIONS:
        optionsBoxes = this.renderOptions();
        break;
      default:
        optionsBoxes = options;
    }

    return {
      elementTitle: elementTitleBox,
      options: optionsBoxes,
      label: labelBox
    };
  };

  renderTextField = (value, name) => (
    <ShortTextBox onChange={this.onChangeElementData} value={value} name={name} />
  );

  renderOptions = () => {
    const { options } = this.props;
    const { newOptionId } = this.state;
    return options.concat({ value: '', id: newOptionId }).map(({ value, id }, i) => {
      return {
        value: this.renderTextField(value, `options[${i}].value`),
        id
      };
    });
  };

  renderNormal = () => {
    const elementData = pick(this.props, ELEMENT_DEFAULT_PROPS);
    return (
      <div className="edit-wrapper">
        <SurveyElement {...elementData} onChange={noop} />
      </div>
    );
  };

  render = () => {
    const { elementComponentId, editing } = this.props;

    if (!editing) {
      return this.renderNormal();
    }

    const dataBoxes = this.getDataRenders();
    const elementData = {
      ...pick(this.props, ELEMENT_DEFAULT_PROPS),
      ...dataBoxes,
      onChange: noop,
      value: '',
      name: ''
    };

    return (
      <div className="edit-wrapper">
        <SurveyElement {...elementData} />
        {elementComponentId === ELEMENT_COMPONENT_IDS.DROPDOWN_OPTIONS && (
          <OrderedList elements={this.renderOptions()} />
        )}
        <EditSurveyElementButtonRow {...this.props} />
      </div>
    );
  };
}
