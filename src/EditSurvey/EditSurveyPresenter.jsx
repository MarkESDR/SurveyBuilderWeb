import React from 'react';
import PropTypes from 'prop-types';

import {
  ELEMENT_DEFAULT_PROP_TYPES,
  ELEMENT_COMPONENT_IDS_ARRAY,
  ELEMENT_COMPONENT_IDS
} from '../Constants/Element';
import { ShortTextBox } from '../Shared';
import SurveyElementSelector from './SurveyElementSelector';
import EditSurveyElement from './EditSurveyElement';

const EditSurveyPresenter = (props) => {
  const {
    elements,
    name,
    editing,
    addElement,
    changeElementData,
    deleteElement,
    onEditSurveyName,
    moveElementDown,
    moveElementUp
  } = props;
  return (
    <div className="edit-survey-container">
      <ShortTextBox name="surveyName" value={name} onChange={onEditSurveyName} />
      {elements.map((element, index) => {
        const { elementComponentId, elementId } = element;
        const elementDown = index + 1 < elements.length ? moveElementDown : undefined;
        const elementUp = index > 0 ? moveElementUp : undefined;
        if (elementComponentId === ELEMENT_COMPONENT_IDS.ELEMENT_SELECTOR) {
          return (
            editing && (
              <SurveyElementSelector
                elementId={elementComponentId}
                key="SurveyElementSelector"
                index={index}
                submit={addElement}
                moveElementUp={elementUp}
                moveElementDown={elementDown}
              />
            )
          );
        }
        return (
          <EditSurveyElement
            {...element}
            editing={editing}
            changeElementData={changeElementData}
            moveElementUp={elementUp}
            moveElementDown={elementDown}
            key={elementId}
            index={index}
            deleteElement={deleteElement}
          />
        );
      })}
    </div>
  );
};

EditSurveyPresenter.propTypes = {
  elements: PropTypes.arrayOf(PropTypes.shape({
    ...ELEMENT_DEFAULT_PROP_TYPES,
    elementComponentId: PropTypes.oneOf(ELEMENT_COMPONENT_IDS_ARRAY)
  })).isRequired,
  name: PropTypes.string.isRequired,
  addElement: PropTypes.func.isRequired,
  changeElementData: PropTypes.func.isRequired,
  onEditSurveyName: PropTypes.func.isRequired,
  moveElementUp: PropTypes.func.isRequired,
  moveElementDown: PropTypes.func.isRequired,
  deleteElement: PropTypes.func.isRequired,
  editing: PropTypes.bool
};

EditSurveyPresenter.defaultProps = {
  editing: true
};

export default EditSurveyPresenter;
