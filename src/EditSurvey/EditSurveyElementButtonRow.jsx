import React from 'react';
import PropTypes from 'prop-types';

import { ButtonRow } from '../Shared';

const EditSurveyElementButtonRow = (props) => {
  const {
    moveElementUp, moveElementDown, deleteElement, elementId
  } = props;
  const buttons = [];
  if (moveElementUp) {
    buttons.push({
      text: 'Move up',
      onClick: () => moveElementUp(elementId)
    });
  }
  if (moveElementDown) {
    buttons.push({
      text: 'Move down',
      onClick: () => moveElementDown(elementId)
    });
  }
  if (deleteElement) {
    buttons.push({
      text: 'Delete Element',
      onClick: () => deleteElement(elementId)
    });
  }
  return <ButtonRow buttons={buttons} />;
};

EditSurveyElementButtonRow.propTypes = {
  elementId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  moveElementUp: PropTypes.func,
  moveElementDown: PropTypes.func,
  deleteElement: PropTypes.func
};

EditSurveyElementButtonRow.defaultProps = {
  elementId: '',
  moveElementUp: undefined,
  moveElementDown: undefined,
  deleteElement: undefined
};

export default EditSurveyElementButtonRow;
