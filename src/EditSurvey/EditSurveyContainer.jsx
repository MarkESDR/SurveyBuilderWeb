import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { assign, findIndex } from 'lodash';

import {
  ELEMENT_DEFAULT_PROP_TYPES,
  ELEMENT_COMPONENT_IDS,
  ELEMENT_COMPONENT_IDS_BY_NAME
} from '../Constants/Element';
import { createSurveyElement, shiftArrayItemIndex } from '../Helper';
import EditSurveyPresenter from './EditSurveyPresenter';

export default class EditSurveyContainer extends Component {
  static propTypes = {
    elements: PropTypes.arrayOf(PropTypes.shape(ELEMENT_DEFAULT_PROP_TYPES)).isRequired,
    name: PropTypes.string.isRequired,
    editing: PropTypes.bool,
    updateElements: PropTypes.func.isRequired,
    updateName: PropTypes.func.isRequired
  };

  static defaultProps = {
    editing: true
  };

  constructor(props) {
    super(props);

    const { elements, name } = props;
    const selector = {
      elementComponentId: ELEMENT_COMPONENT_IDS.ELEMENT_SELECTOR,
      elementTitle: 'Element Selector',
      elementId: ELEMENT_COMPONENT_IDS.ELEMENT_SELECTOR
    };
    this.state = {
      elements: elements.concat(selector),
      name
    };
  }

  onEditSurveyName = (e) => {
    const name = e.target.value;
    this.setState(
      {
        ...this.state,
        name
      },
      () => this.props.updateName(name)
    );
  };

  updateElements = (elements) => {
    const elementsWithoutSelector = elements.filter((element) => {
      return element.elementComponentId !== ELEMENT_COMPONENT_IDS.ELEMENT_SELECTOR;
    });
    this.setState(
      {
        ...this.state,
        elements
      },
      () => this.props.updateElements(elementsWithoutSelector)
    );
  };

  addElement = (elementName) => {
    const { elements } = this.state;

    const indexOfSelector = findIndex(elements, {
      elementComponentId: ELEMENT_COMPONENT_IDS.ELEMENT_SELECTOR
    });

    const newElement = createSurveyElement(ELEMENT_COMPONENT_IDS_BY_NAME[elementName]);
    elements.splice(indexOfSelector, 0, newElement);

    this.updateElements(elements);
  };

  changeElementData = (elementId, newData) => {
    const { elements } = this.state;

    const indexOfElement = findIndex(elements, { elementId });
    const newElement = assign(elements[indexOfElement], newData);
    elements.splice(indexOfElement, 1, newElement);

    this.updateElements(elements);
  };

  deleteElement = (elementId) => {
    const { elements } = this.state;

    const indexOfElement = findIndex(elements, { elementId });
    elements.splice(indexOfElement, 1);

    this.updateElements(elements);
  };

  moveElementIndex = (elementId, delta) => {
    const { elements } = this.state;

    const indexOfElement = findIndex(elements, { elementId });

    this.updateElements(shiftArrayItemIndex(elements, indexOfElement, delta));
  };

  moveElementIndexUp = elementId => this.moveElementIndex(elementId, 1);

  moveElementIndexDown = elementId => this.moveElementIndex(elementId, -1);

  render() {
    const { editing } = this.props;
    const { elements, name } = this.state;
    return (
      <EditSurveyPresenter
        elements={elements}
        name={name}
        editing={editing}
        addElement={this.addElement}
        changeElementData={this.changeElementData}
        deleteElement={this.deleteElement}
        onEditSurveyName={this.onEditSurveyName}
        moveElementUp={this.moveElementIndexDown}
        moveElementDown={this.moveElementIndexUp}
      />
    );
  }
}
