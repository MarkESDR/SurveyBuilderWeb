import React, { Component } from 'react';
import {
  SurveyCheckBox,
  SurveyDropdownOptions,
  SurveyParagraphEdit,
  SurveyRadioOptions,
  SurveyShortTextEdit
} from '../survey/WrappedElements';
import { SurveyPresenter } from '../survey/Presenter';

const OPTIONS = ['This is an option', 'So is this one', 'These are arbitrary', 'HYUK HYUK'];

class SurveyComponentDummies extends Component {
  state = {
    items: [
      {
        component: SurveyCheckBox,
        compoentId: 'ChekinBox',
        checked: false,
        label: 'This is a CheckBox'
      },
      {
        component: SurveyDropdownOptions,
        componentId: 'dropdrop',
        value: '',
        options: OPTIONS
      },
      {
        component: SurveyParagraphEdit,
        componentId: 'prrrr',
        value: ''
      },
      {
        component: SurveyRadioOptions,
        compoentId: 'radiouou',
        value: '',
        options: OPTIONS
      },
      {
        component: SurveyShortTextEdit,
        componentId: 'sttttt',
        value: ''
      }
    ]
  };

  onChange = (e, component) => {
    const updatedItems = this.state.items.map((item) => {
      if (item.component === component) {
        return {
          ...item,
          checked: e.target.checked,
          value: e.target.value
        };
      }
      return item;
    });
    this.setState({
      ...this.state,
      items: updatedItems
    });
  };

  render = () => (
    <div>
      Hello
      <SurveyPresenter components={this.state.items} onChange={this.onChange} />
    </div>
  );
}

module.exports = { SurveyComponentDummies };
