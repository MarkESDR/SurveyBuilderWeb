import PropTypes from 'prop-types';

import { ELEMENT_DEFAULT_PROP_TYPES } from './Element';

export const SURVEY_DEFAULT_PROP_TYPES = {
  elements: PropTypes.arrayOf(PropTypes.shape(ELEMENT_DEFAULT_PROP_TYPES)),
  surveyId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  name: PropTypes.string,
  createdAt: PropTypes.number
};

export const SURVEY_DEFAULT_VALUES = {
  elements: [],
  name: 'Unnamed'
};
