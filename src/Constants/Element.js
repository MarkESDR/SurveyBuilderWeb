import PropTypes from 'prop-types';
import { mapKeys } from 'lodash';
import { CheckBox, DropdownOptions, ParagraphBox, RadioOptions, ShortTextBox } from '../Shared';
import SurveyElementSelector from '../EditSurvey/SurveyElementSelector';

export const ELEMENT_COMPONENT_IDS = {
  ELEMENT_SELECTOR: -1,
  CHECK_BOX: 1,
  DROPDOWN_OPTIONS: 2,
  PARAGRAPH_BOX: 3,
  RADIO_OPTIONS: 4,
  SHORT_TEXT_BOX: 5
};

export const ELEMENT_COMPONENT_IDS_ARRAY = Object.values(ELEMENT_COMPONENT_IDS);

export const ELEMENT_COMPONENT_NAMES = {
  CHECK_BOX: 'Checkbox',
  DROPDOWN_OPTIONS: 'Dropdown',
  PARAGRAPH_BOX: 'Paragraph',
  RADIO_OPTIONS: 'Radio_Buttons',
  SHORT_TEXT_BOX: 'Short_Text'
};

export const ELEMENT_COMPONENT_NAMES_BY_ID = mapKeys(
  ELEMENT_COMPONENT_NAMES,
  (value, key) => ELEMENT_COMPONENT_IDS[key]
);

export const ELEMENT_COMPONENT_IDS_BY_NAME = mapKeys(
  ELEMENT_COMPONENT_IDS,
  (value, key) => ELEMENT_COMPONENT_NAMES[key]
);

export const ELEMENT_COMPONENT_NAMES_ARRAY = Object.values(ELEMENT_COMPONENT_NAMES);

export const ELEMENT_COMPONENTS_BY_ID = {
  [ELEMENT_COMPONENT_IDS.ELEMENT_SELECTOR]: SurveyElementSelector,
  [ELEMENT_COMPONENT_IDS.CHECK_BOX]: CheckBox,
  [ELEMENT_COMPONENT_IDS.DROPDOWN_OPTIONS]: DropdownOptions,
  [ELEMENT_COMPONENT_IDS.PARAGRAPH_BOX]: ParagraphBox,
  [ELEMENT_COMPONENT_IDS.RADIO_OPTIONS]: RadioOptions,
  [ELEMENT_COMPONENT_IDS.SHORT_TEXT_BOX]: ShortTextBox
};

export const ELEMENT_COMPONENTS_ARRAY = Object.values(ELEMENT_COMPONENTS_BY_ID);

export const ELEMENTS_WITH_OPTIONS = [
  ELEMENT_COMPONENT_IDS.DROPDOWN_OPTIONS,
  ELEMENT_COMPONENT_IDS.RADIO_OPTIONS
];

export const ELEMENT_DEFAULT_PROP_TYPES = {
  elementComponentId: PropTypes.oneOf(Object.values(ELEMENT_COMPONENT_IDS)),
  elementTitle: PropTypes.node.isRequired,
  elementId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.node,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
  })),
  label: PropTypes.node,
  value: PropTypes.string
};

export const ELEMENT_DEFAULT_PROPS = Object.keys(ELEMENT_DEFAULT_PROP_TYPES);

export const BLANK_PROP_VALUES = {
  options: [],
  label: '',
  value: ''
};

export const ELEMENT_DEFAULT_PROP_VALUES = {
  elementTitle: 'Insert your title here',
  label: 'Insert label here',
  options: ['Mark', 'Emmanuel', 'Savellano', 'Del', 'Rosario', 'Alan', 'Cortes', 'Chris', 'Colon']
};
