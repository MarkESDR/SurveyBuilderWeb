import shortid from 'shortid';

import { ELEMENT_DEFAULT_PROP_VALUES } from './Constants/Element';
import { SURVEY_DEFAULT_VALUES } from './Constants/Survey';

export function generateId() {
  // return `_${Math.random().toString(36).substr(2, 9)}`;
  // return `${Date.now()}`;
  return shortid.generate();
}

export function createSurveyElement(
  elementComponentId,
  options = [],
  label = ELEMENT_DEFAULT_PROP_VALUES.label,
  elementTitle = ELEMENT_DEFAULT_PROP_VALUES.elementTitle
) {
  return {
    elementComponentId,
    elementTitle,
    elementId: generateId(),
    options,
    label
  };
}

export function createSurvey(
  name = SURVEY_DEFAULT_VALUES.name,
  elements = SURVEY_DEFAULT_VALUES.elements
) {
  return {
    name,
    elements,
    surveyId: generateId(),
    createdAt: Date.now()
  };
}

function shiftArrayItemUp(arr, index, numUp) {
  const before = arr.slice(0, index);
  const element = arr.slice(index, index + 1);
  const shift = arr.slice(index + 1, index + numUp + 1);
  const after = arr.slice(index + numUp + 1);
  return [].concat(before, shift, element, after);
}

function shiftArrayItemDown(arr, index, numDown) {
  const before = arr.slice(0, index - numDown);
  const shift = arr.slice(index - numDown, index);
  const element = arr.slice(index, index + 1);
  const after = arr.slice(index + 1);
  return [].concat(before, element, shift, after);
}

export function shiftArrayItemIndex(arr, index, delta) {
  let newArr = arr;
  if (delta > 0) {
    newArr = shiftArrayItemUp(arr, index, delta);
  } else if (delta < 0) {
    newArr = shiftArrayItemDown(arr, index, index + delta < 0 ? index : delta * -1);
  }
  return newArr;
}
