import React from 'react';
import PropTypes from 'prop-types';
import { omit } from 'lodash';

import { SURVEY_DEFAULT_PROP_TYPES } from '../Constants/Survey';

const SurveyMenuPresenter = (props) => {
  const { surveys, onEditSurvey } = props;
  return (
    <table>
      <tbody>
        <tr>
          <th>Name</th>
          <th />
          <th>Created at</th>
        </tr>
        {surveys.map((survey) => {
          const { surveyId, name, createdAt } = survey;
          const createAtDate = new Date(createdAt);
          return (
            <tr key={surveyId}>
              <td>{name}</td>
              <td>
                <button onClick={onEditSurvey} name={surveyId}>
                  Edit
                </button>
              </td>
              <td>{createAtDate.toString()}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

SurveyMenuPresenter.propTypes = {
  surveys: PropTypes.arrayOf(PropTypes.shape(omit(SURVEY_DEFAULT_PROP_TYPES, 'elements'))).isRequired,
  onEditSurvey: PropTypes.func.isRequired
};

export default SurveyMenuPresenter;
