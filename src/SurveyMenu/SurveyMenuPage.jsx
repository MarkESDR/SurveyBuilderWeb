import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { omit } from 'lodash';

import { Button } from '../Shared';
import { SURVEY_DEFAULT_PROP_TYPES } from '../Constants/Survey';
import SurveyMenuContainer from './SurveyMenuContainer';

class SurveyMenuPage extends Component {
  static propTypes = {
    surveys: PropTypes.arrayOf(PropTypes.shape(omit(SURVEY_DEFAULT_PROP_TYPES, 'elements'))).isRequired,
    surveysPerPage: PropTypes.number,
    onCreateSurvey: PropTypes.func.isRequired,
    onEditSurvey: PropTypes.func.isRequired
  };

  static defaultProps = {
    surveysPerPage: 10
  };

  state = {
    currentPage: 0
  };

  changePage = (newPageIndex) => {};

  render() {
    const { onCreateSurvey, onEditSurvey, surveys } = this.props;
    return (
      <div>
        <Button text="Create blank survey" onClick={onCreateSurvey} />
        <SurveyMenuContainer surveys={surveys} onEditSurvey={onEditSurvey} />
      </div>
    );
  }
}

export default SurveyMenuPage;
