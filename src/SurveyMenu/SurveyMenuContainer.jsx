import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { SURVEY_DEFAULT_PROP_TYPES } from '../Constants/Survey';
import SurveyMenuPresenter from './SurveyMenuPresenter';

class SurveyMenuContainer extends Component {
  static propTypes = {
    surveys: PropTypes.arrayOf(PropTypes.shape(SURVEY_DEFAULT_PROP_TYPES)).isRequired,
    onOpenSurvey: PropTypes.func.isRequired,
    onDeleteSurvey: PropTypes.func.isRequired,
    onEditSurvey: PropTypes.func.isRequired
  };

  onEditSurvey = e => this.props.onEditSurvey(e.target.name);

  render() {
    const { surveys } = this.props;
    return <SurveyMenuPresenter surveys={surveys} onEditSurvey={this.onEditSurvey} />;
  }
}

export default SurveyMenuContainer;
